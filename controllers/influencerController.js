// const asyncHandler = require("express-async-handler");
// const Influencer = require("../models/influencerModel");

// // @desc    Get about
// // @route   GET /api/about
// // @access  Users and Admins
// const getInfluencer = asyncHandler(async (req, res) => {
//   const influencers = await Influencer.find({}).populate("voters");
//   res.json(influencers);
// });

// // @desc      Get influencer by ID
// // @route     GET /api/influencer/:id
// // @access    Private/token
// const getInfluencerById = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);

//   if (influencer) {
//     res.json(influencer);
//   } else {
//     res.status(404);
//     throw new Error("influencer not found!");
//   }
// });

// const setInfluencer = asyncHandler(async (req, res) => {
//   const influencer = new Influencer({
//     name: req.body.name,
//     image: req.file.path,
//   });

//   if (!influencer) {
//     res.status(400).json({ message: err });
//   }

//   const setInfluen = await influencer.save();
//   res.status(201).json(setInfluen);
// });

// // @desc    Update about
// // @route   PUT /api/about/:id
// // @access  Private
// const updateInfluencer = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);

//   if (!influencer) {
//     res.status(400);
//     throw new Error("influencer not found !");
//   }

//   const updateInfluencer = await Influencer.findByIdAndUpdate(
//     req.params.id,
//     req.body,
//     {
//       new: true,
//     }
//   );

//   res.status(200).json(updateInfluencer);
// });

// // @desc      Delete Donor by ID
// // @route     DELETE /api/donors/:id
// // @access    Private/Donor
// const deleteInfluencer = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);
//   if (influencer) {
//     await influencer.remove();
//     res.json({ message: "Influencer Removed" });
//   } else {
//     res.status(404);
//     throw new Error("Influencer not found!");
//   }
// });


// // @desc    Get about
// // @route   GET /api/about
// // @access  Users and Admins
// const getInf = asyncHandler(async (req, res) => {
//   const influencers = await Influencer.find({});
//   res.json(influencers);
// });

// module.exports = {
//   getInfluencer,
//   setInfluencer,
//   updateInfluencer,
//   deleteInfluencer,
//   getInfluencerById,
//   getInf
// };


const asyncHandler = require("express-async-handler");
const Influencer = require("../models/influencerModel");

// @desc    Get about
// @route   GET /api/about
// @access  Users and Admins
const getInfluencer = asyncHandler(async (req, res) => {
  const influencers = await Influencer.find({}).populate("voters");
  res.json(influencers);
});

const setInfluencer = asyncHandler(async (req, res) => {
  const influencer = new Influencer({
    name: req.body.name,
    image: req.file.path,
  });

  if (!influencer) {
    res.status(400).json({ message: err });
  }

  const setInfluen = await influencer.save();
  res.status(201).json(setInfluen);
});

// @desc    Update about
// @route   PUT /api/about/:id
// @access  Private
const updateInfluencer = asyncHandler(async (req, res) => {
  const influencer = await Influencer.findById(req.params.id);

  if (!influencer) {
    res.status(400);
    throw new Error("influencer not found !");
  }

  const updateInfluencer = await Influencer.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true
    }
  );

  res.status(200).json(updateInfluencer);
});

// @desc      Delete Donor by ID
// @route     DELETE /api/donors/:id
// @access    Private/Donor
const deleteInfluencer = asyncHandler(async (req, res) => {
  const influencer = await Influencer.findById(req.params.id);
  if (influencer) {
    await influencer.remove();
    res.json({ message: "Influencer Removed" });
  } else {
    res.status(404);
    throw new Error("Influencer not found!");
  }
});

module.exports = {
  getInfluencer,
  setInfluencer,
  updateInfluencer,
  deleteInfluencer
};
