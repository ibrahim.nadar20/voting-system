// const asyncHandler = require("express-async-handler");
// const Influencer = require("../models/influencerModel");

// // @desc    Get about
// // @route   GET /api/about
// // @access  Users and Admins
// const getInfluencer = asyncHandler(async (req, res) => {
//   const influencers = await Influencer.find({}).populate("voters");
//   res.json(influencers);
// });

// // @desc      Get influencer by ID
// // @route     GET /api/influencer/:id
// // @access    Private/token
// const getInfluencerById = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);

//   if (influencer) {
//     res.json(influencer);
//   } else {
//     res.status(404);
//     throw new Error("influencer not found!");
//   }
// });

// const setInfluencer = asyncHandler(async (req, res) => {
//   const influencer = new Influencer({
//     name: req.body.name,
//     image: req.file.path,
//   });

//   if (!influencer) {
//     res.status(400).json({ message: err });
//   }

//   const setInfluen = await influencer.save();
//   res.status(201).json(setInfluen);
// });

// // @desc    Update about
// // @route   PUT /api/about/:id
// // @access  Private
// const updateInfluencer = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);

//   if (!influencer) {
//     res.status(400);
//     throw new Error("influencer not found !");
//   }

//   const updateInfluencer = await Influencer.findByIdAndUpdate(
//     req.params.id,
//     req.body,
//     {
//       new: true,
//     }
//   );

//   res.status(200).json(updateInfluencer);
// });

// // @desc      Delete Donor by ID
// // @route     DELETE /api/donors/:id
// // @access    Private/Donor
// const deleteInfluencer = asyncHandler(async (req, res) => {
//   const influencer = await Influencer.findById(req.params.id);
//   if (influencer) {
//     await influencer.remove();
//     res.json({ message: "Influencer Removed" });
//   } else {
//     res.status(404);
//     throw new Error("Influencer not found!");
//   }
// });


// // @desc    Get about
// // @route   GET /api/about
// // @access  Users and Admins
// const getInf = asyncHandler(async (req, res) => {
//   const influencers = await Influencer.find({});
//   res.json(influencers);
// });

// module.exports = {
//   getInfluencer,
//   setInfluencer,
//   updateInfluencer,
//   deleteInfluencer,
//   getInfluencerById,
//   getInf
// };


const asyncHandler = require("express-async-handler");
const Logo = require("../models/logoModel");





// ----------------------------------------------------Logo header
// @desc    Get about
// @route   GET /api/about
// @access  Users and Admins
const getLogo = asyncHandler(async (req, res) => {
  const logos = await Logo.find({});
  res.json(logos);
});

const setLogo = asyncHandler(async (req, res) => {
  const logo = new Logo({
    
    logo_header: req.file.path,
    
  });

  if (!logo) {
    res.status(400).json({ message: err });
  }

  const setLogo = await logo.save();
  res.status(201).json(setLogo);
});




// @desc    Update about
// @route   PUT /api/about/:id
// @access  Private
const updateLogo = asyncHandler(async (req, res) => {
  const logo = await Logo.findById(req.file.path); 
//   const logo = await Logo.findById(req.params.id);
  if (!logo) {
    res.status(400);
    throw new Error("Logo not found !");
  }

  const updateLogo = await Logo.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true
    }
  );

  res.status(200).json(updateLogo);
});

// @desc      Delete Donor by ID
// @route     DELETE /api/donors/:id
// @access    Private/Donor
const deleteLogo = asyncHandler(async (req, res) => {
  const logo = await Logo.findById(req.file.path);
  if (logo) {
    await logo.remove();
    res.json({ message: "logo Removed" });
  } else {
    res.status(404);
    throw new Error("logo not found!");
  }
});

module.exports = {
  getLogo,
  setLogo,
  updateLogo,
  deleteLogo
};
