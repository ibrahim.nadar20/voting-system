const asyncHandler = require("express-async-handler");
const Logocard = require("../models/logocardModel");


// @desc    Get about
// @route   GET /api/about
// @access  Users and Admins
const getLogocard = asyncHandler(async (req, res) => {
  const logocards = await Logocard.find({});
  res.json(logocards);
});

const setLogocard = asyncHandler(async (req, res) => {
  const logocard = new Logocard({
    logo_card: req.file.path,
  });

  if (!logocard) {
    res.status(400).json({ message: err });
  }

  const setLogocard = await logocard.save();
  res.status(201).json(setLogocard);
});

module.exports = {
  getLogocard,
  setLogocard,
};
