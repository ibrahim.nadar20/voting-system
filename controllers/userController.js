// const jwt = require("jsonwebtoken");
// const bcrypt = require("bcryptjs");
// const asyncHandler = require("express-async-handler");
// const User = require("../models/userModel");
// const Influencer = require("../models/influencerModel");
// const emailValidator = require("deep-email-validator");

// // @desc    Register new user
// // @route   POST /api/users/register
// // @access  Public
// const registerUser = asyncHandler(async (req, res) => {
//   const { name, email, password } = req.body;

//   if (!name || !email || !password) {
//     res.status(400);
//     throw new Error("Please add all fields");
//   }

//   if (password.length < 8) {
//     res.status(400);
//     throw new Error("Password should be more than 8 characters ");
//   }

//   // Check if user exists
//   const userExists = await User.findOne({ email });

//   if (userExists) {
//     res.status(400);
//     throw new Error("User already exists");
//   }

//   // if (!isEmail(email)) {
//   //   res.status(400);
//   //   throw new Error( "Please provide a valid email adress" );
//   // }
//   // if (!isEmail(email)) {
//   //   res.status(400).json({ message: "Please provide a valid email adress" });
//   //   return;
//   // }

//   // const emailExists = await email.isEmail({email});

//   //   if (emailExists) {
//   //     res.status(400);
//   //     throw new Error("Please provide an appropriate email  ");
//   //   }
//   // if (!req.body.email.isEmail()) {
//   //   errors.push({ msg: 'Please provide an appropriate email' });
//   // }

//   // Hash password
//   const salt = await bcrypt.genSalt(10);
//   const hashedPassword = await bcrypt.hash(password, salt);

//   // Create user
//   const user = await User.create({
//     name,
//     email,
//     password: hashedPassword,
//   });

//   if (user) {
//     res.status(201).json({
//       _id: user.id,
//       name: user.name,
//       email: user.email,
//       vote: user.vote,
//       token: generateToken(user._id),
//     });
//   } else {
//     res.status(400);
//     throw new Error("Invalid user data");
//   }
// });

// // @desc    Authenticate a user
// // @route   POST /api/users/login
// // @access  Public
// const loginUser = asyncHandler(async (req, res) => {
//   const { email, password } = req.body;

//   if (!email || !password) {
//     res.status(400);
//     throw new Error("Please add all fields");
//   }

//   if (password.length < 8) {
//     res.status(400);
//     throw new Error("Password should be more than 8 characters ");
//   }

//   // Check for user email
//   const user = await User.findOne({ email });

//   if (user && (await bcrypt.compare(password, user.password))) {
//     res.json({
//       _id: user.id,
//       name: user.name,
//       email: user.email,
//       vote: user.vote,

//       token: generateToken(user._id),
//     });
//   } else {
//     res.status(400);
//     throw new Error("Invalid credentials");
//   }
// });

// // @desc    Get user data
// // @route   GET /api/users/me
// // @access  Private
// const getMe = asyncHandler(async (req, res) => {
//   res.status(200).json(req.user);
// });

// // const vote = asyncHandler(async (req, res) => {
// //   console.log(req.user);
// //   const { id_inf } = req.body;
// //   const { user_id } = req.user._id;
// //   const influencer = await Influencer.findOne({ id_inf });
// //   const user = await User.findOne({ user_id });
// //   if (!influencer || !user) {
// //     return res.status(401).json({ error: "influencer not found!" });
// //   }
// //   if (influencer.voters.includes(req.user._id)) {
// //     return res.status(401).json({ error: "you are voted before!!" });
// //   }
// //   influencer.voters.push(req.user._id);
// //   const after = await influencer.save();
// //   res.status(200).json({ success: "voted!" });
// // });

// const vote = asyncHandler(async (req, res) => {
//   console.log(req.user)
//   const { id_inf } = req.body;
//   const { user_id } = req.user._id;
//   const influencer = await Influencer.findOne({ id_inf });
//   const user = await User.findOne({ user_id });
//   if (!influencer || !user) {
//     return res.status(401).json({ error: "influencer not found!" });
//   }
//   if (user.isVote) {
//     return res.status(401).json({ error: "you are voted before!" });
//   }
//   influencer.voters.push(req.user._id);
//   const after = await influencer.save();
//   user.isVote=true;
//   const userafter = await user.save();
//   res.status(200).json({ success: "voted!" });
// });

// // @desc    Get all users
// // @route   GET /api/users/
// // @access  Private
// const getUsers = asyncHandler(async (req, res) => {
//   const users = await User.find({});
//   res.json(users);
// });

// // @desc      Get user by ID
// // @route     GET /api/users/:id
// // @access    Private/Admin
// const getUserById = asyncHandler(async (req, res) => {
//   const user = await User.findById(req.params.id).select("-password");

//   if (user) {
//     res.json(user);
//   } else {
//     res.status(404);
//     throw new Error("User not found!");
//   }
// });

// // @desc      Delete Donor by ID
// // @route     DELETE /api/donors/:id
// // @access    Private/Donor
// const deleteUser = asyncHandler(async (req, res) => {
//   const user = await User.findById(req.params.id);
//   if (user) {
//     await user.remove();
//     res.json({ message: "User Removed" });
//   } else {
//     res.status(404);
//     throw new Error("User not found!");
//   }
// });

// // Generate JWT
// const generateToken = (id) => {
//   return jwt.sign({ id }, process.env.JWT_SECRET, {
//     expiresIn: "30d",
//   });
// };

// module.exports = {
//   registerUser,
//   loginUser,
//   getMe,
//   getUsers,
//   deleteUser,
//   getUserById,
//   vote,
// };

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const asyncHandler = require("express-async-handler");
const User = require("../models/userModel");
const Influencer = require("../models/influencerModel");
const { request, response } = require("express");

// @desc    Register new user
// @route   POST /api/users/register
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
  const { name, phone, email, password } = req.body;

  if (!name || !phone || !email || !password) {
    res.status(400);
    throw new Error("Please add all fields");
  }

  // Check if user exists
  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400);
    throw new Error("User already exists");
  }

  const phoneExists = await User.findOne({ phone });
  if (phoneExists) {
    res.status(400);
    throw new Error("Phone number already exists");
  }
  // Hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  // Create user
  const user = await User.create({
    name,
    email,
    phone,
    password: hashedPassword,
  });

  if (user) {
    res.status(201).json({
      _id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      vote: user.vote,
      token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error("Invalid user data");
  }
});

// @desc    Authenticate a user
// @route   POST /api/users/login
// @access  Public
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  // Check for user email
  const user = await User.findOne({ email });

  if (user && (await bcrypt.compare(password, user.password))) {
    res.json({
      _id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      vote: user.vote,

      token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error("Invalid credentials");
  }
  console.log(response);
});

// @desc    Get user data
// @route   GET /api/users/me
// @access  Private
const getMe = asyncHandler(async (req, res) => {
  res.status(200).json(req.user);
});

const vote = asyncHandler(async (req, res) => {
  const { id_inf } = req.body;
  const influencer = await Influencer.findById(id_inf);
  const user = await User.findById(req.user._id);
  console.log(user);
  if (!influencer || !user) {
    return res.status(401).json({ error: "influencer not found!" });
  }
  if (user.isVote === true) {
    return res.status(401).json({ error: "you are voted before!" });
  }
  influencer.voters.push(req.user._id);
  const after = await influencer.save();
  user.isVote = true;
  const userafter = await user.save();
  res.status(200).json({ success: "voted!" });
  console.log(request.body);
});

// @desc    Get all users
// @route   GET /api/users/
// @access  Private
const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({});
  res.json(users);
});

// @desc      Get user by ID
// @route     GET /api/users/:id
// @access    Private/Admin
const getUserById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select("-password");

  if (user) {
    res.json(user);
  } else {
    res.status(404);
    throw new Error("User not found!");
  }
});

const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);
  if (user) {
    await user.remove();
    res.json({ message: "User Removed" });
  } else {
    res.status(404);
    throw new Error("User not found!");
  }
});

// Generate JWT
const generateToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: "30d",
  });
};

module.exports = {
  registerUser,
  loginUser,
  getMe,
  getUsers,
  getUserById,
  deleteUser,
  vote,
};
