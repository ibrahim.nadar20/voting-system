import React from "react";
import "./inf.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function Inf() {
  //   const URLVotes = process.env.REACT_APP_URL + "/inf/";
  const [votes, setVotes] = useState([]);
  useEffect(() => {
    getVoters([]);
  });

  const getVoters = async () => {
    const token = localStorage.getItem("token");
    try {
      const response = await axios.get(`/api/inf/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // console.log(response.data);
      setVotes(response.data);
      // console.log(response.data);
    } catch (err) {
      // console.log('er',err);
    }
  };

  const length = getVoters.length;

  const navigate = useNavigate();
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/login");
    }
  });

  return (
    <>
      <h1 className="admin-t">Admin</h1>

      <table id="customers">
        <tr>
          <th>ID</th>
          <th>Influencer</th>
          <th>Votes</th>
        </tr>

        {votes.map((vote) => (
          <tr key={vote._id}>
            <td>{vote._id}</td>
            <td>{vote.name}</td>
            <td>{vote.voters.length}</td>
          </tr>
        ))}
      </table>
    </>
  );
}

export default Inf;
