import React from "react";
import "./footer.css";
import kingco from "../../Assets/kingco.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faYoutube,
  faFacebook,
  faGoogle,
  faInstagram,
  faTiktok,
} from "@fortawesome/free-brands-svg-icons";
function Footer() {
  return (
    <footer>
      <div className="footer-content">
        <h3>KinGco Media Agency</h3>
        <p>
          we are social media agency, located in Lebanon, Saida Al Janoub near
          LIU Uinversity. We are driven by creativity. We create innovative
          things to help you achieve better results and consolidate yourself in
          the market.<br></br> <span className="phone">+961 81 070 009</span>
        </p>
        <ul className="socials">
          <a
            href="https://www.instagram.com/kingco.socialmedia/?hl=en"
            style={{ target: "blank" }}
          >
            <img src={kingco} className="logo-kingco" alt="3anbar_restaurant" />
          </a>
        </ul>
        <ul className="socials">
          <li>
            <a href="https://www.tiktok.com/@kingco.media">
              <i className="tiktok">
                <FontAwesomeIcon icon={faTiktok} size="1x" />
              </i>
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/kingco.socialmedia/?hl=en">
              <i className="instagram">
                {" "}
                <FontAwesomeIcon icon={faInstagram} size="1x" />
              </i>
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/profile.php?id=100078402746981&paipv=0&eav=AfYqzXsoyfhwC7-30XCyBwIEVkQ1dEfwVj5DIH06ZuFQ-LytEy9Atdl0IqwwVgrwG_k">
              <i className="facebook">
                {" "}
                <FontAwesomeIcon icon={faFacebook} size="1x" />
              </i>
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com/watch?v=NoKdVf_dGmg">
              <i className="youtube">
                <FontAwesomeIcon icon={faYoutube} size="1x" />
              </i>
            </a>
          </li>
          <li>
            <a href="https://kingcomedia.com">
              <i className="google">
                {" "}
                <FontAwesomeIcon icon={faGoogle} size="1x" />
              </i>
            </a>
          </li>
        </ul>
      </div>
      <div className="footer-bottom">
        <p>
          copyright &copy;
          <a href="https://kingcomedia.com" className="href">
            KinGcoMedia 2023
          </a>{" "}
        </p>
      </div>
    </footer>
  );
}

export default Footer;




// import React from "react";
// import "./footer.css";
// import kingco from "../../Assets/kingco.png";
// function Footer() {
//   return (
//     <footer className="footer-distributed">
//       <div className="footer-left">
//         {/* <h3>
//           Company<span>logo</span>
//         </h3> */}
//         <a
//           href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D"
//           style={{ target: "blank" }}
//         >
//           <img src={kingco} className="logo-f" alt="3anbar_restaurant" />
//         </a>

//         <p className="footer-links">
//           {/* <a href="#" className="link-1">
//             Home
//           </a>

//           <a href="#">Blog</a>

//           <a href="#">Pricing</a>

//           <a href="#">About</a>

//           <a href="#">Faq</a>

//           <a href="#">Contact</a> */}
//           <a className="link-1" href="https://kingcomedia.com/">
//             KInGco media Agency{" "}
//           </a>
//         </p>
//         {/* <br />
//           <br />
//           <a href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D">
//             <p className="footer-company-name">Golden_International© 2015</p> 
//             </a> */}
//       </div>

//       <div className="footer-center">
//         <div>
//           <i className="fa fa-map-marker"></i>
//           <p>
//             {/* <span> Koraytem Street</span>Beirut-Lebanon */}
//             <span> Near LIU Street</span>Saida-Lebanon
//           </p>
//         </div>

//         <div>
//           <i className="fa fa-phone"></i>
//           <p>+961 81 070 009</p> <br />
//           <i className="fa fa-phone"></i>
//           <p>+961 03 066 391</p>
//           <br /> <br />
//         </div>

//         <div>
//           <i className="fa fa-envelope"></i>
//           <p>
//             <a href="https://www.instagram.com/kingco.socialmedia/?hl=en">
//               Powered by KinGco Media©2023
//             </a>
//           </p>
//         </div>
//       </div>

//       <div className="footer-right">
//         <div className="footer-center">
//           <div>
//             <i className="fa fa-map-marker"></i>
//             <p>
//             <a href="https://www.instagram.com/kingco.socialmedia/?hl=en">
//                 <span>KinGco media</span>
//               </a>
//               <a href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D">
//                 <span>Golden Int.</span>
//               </a>
//               <a href="https://www.instagram.com/khabar3ajelofficial/?igshid=YmMyMTA2M2Y%3D">
//                 <span>Khabar3ajel</span>
//               </a>
//               <a href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D">
//                 <span>Beryte prod.</span>
//               </a>
//               <a href="https://www.instagram.com/sparksgroup/?hl=en">
//                 <span>Spark's</span>
//               </a>
//               <br></br>
//               <a href="mailto:info@kingco.media">info@kingco.media</a>
//             </p>
//           </div>

//           <div>
//             <i className="fa fa-phone"></i>
//             {/* <p>+961 81 070 009</p> */}
//             {/* <br /><br /> */}
//           </div>

//           <div>
//             <i className="fa fa-envelope"></i>
//             {/* <p>
//               <a
//                 href="mailto:info@kingco.media"
//               >
//                 info@kingco.media
//               </a>
              
//             </p> */}
//           </div>
//         </div>
//         {/* <p className="footer-company-about">
//         <span>About the company</span>
//         We are driven by creativity. We create innovative things to help you
//         achieve better results and consolidate yourself in the market. We do
//         unique designs.
//       </p> */}

//         {/* <div className="footer-icons">
//         <a href="#"><i className="fa fa-facebook"></i></a>
//         <a href="#"><i className="fa fa-twitter"></i></a>
//         <a href="#"><i className="fa fa-linkedin"></i></a>
//         <a href="#"><i className="fa fa-github"></i></a>
//       </div>   */}
//       </div>
//     </footer>
//   );
// }

// export default Footer;
