import React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import "../../pages/home.css";
// import kingco from "../../Assets/kingco.png";
// import khabar from "../../Assets/khabar.png";
// import Beryte from "../../Assets/done.png";
// import gi from "../../Assets/gi.png";
// import spark from "../../Assets/spark.png";
function Logos() {
  let token = localStorage.getItem("token");

  const [logos, setLogos] = useState([]);

  const getLogos = async () => {
    // const {  image } = props;
    try {
      const response = await axios.get(`/api/logo/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setLogos(response.data);
      // console.log(response.data);
    } catch (err) {
      // console.log('er',err);
    }

    // console.log(logos);
  };
  useEffect(() => {
    getLogos();
  }, []);

  return (
    <div className="logos">
      {logos.map((logo) => (
        <a key={logo._id} href="http://www.kingcomedia.com" >
          <img
            key={logo._id}
            src={`/${logo.logo_header}`}
            alt="golden international"
          />
        </a>
      ))}
      
    </div>
  );
}

export default Logos;
