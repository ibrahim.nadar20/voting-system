import React, { useState } from "react";
// import useEffect from "react";
import "./login.css";
import { useNavigate } from "react-router-dom";
import { Button, Box, TextField, Alert } from "@mui/material";
import axios from "axios";
// import URL from "../../helpers/Url";
import kingco from "../../Assets/logo.png";

// const URLLogin = process.env.REACT_APP_URL + "/users/login";

const Login = () => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [error, setError] = useState(false);
  // const [setError] = useState(false);
  let navigate = useNavigate();

  // const [type, setType] = useState("password");

  const loginFunc = async (e) => {
    console.log("before prevent");
    e.preventDefault();
    setGeneralError("");
    setEmailError("");
    setPasswordError("");
    setError("");
    console.log("after prevent");
    // `${URLLogin}`

    try {
      const response = await axios.post(
        `/api/users/login`,
        {
          email: email,
          password: password,
        }
      );
      // console.log("email ", email);
      // console.log("password ", password);

      const res = response.data;
      // console.log("hii", res);
      localStorage.setItem("token", res.token);

      navigate("/home");
      // console.log("hello", error);
    } catch (error) {
      console.log(error);
    }
  };

  const user = localStorage.getItem("token");
  //kermal bas yerj3 bl sahem lawara ydal bl home page (security)
  // useEffect(() => {
  //   if (user) navigate("/home");
  // }, []);

  return (
    <div className="loginContainer2">
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <div className="loginContainerform2">
          {generalError && (
            <Alert
              severity="error"
              sx={{
                height: "40px",
                transform: "scale(1.3)",
                width: "10rem",
                position: "absolute",
                top: "20%",
              }}
            >
              {generalError}
            </Alert>
          )}

          <img src={kingco} className="be3" alt="GI" />
          {/* <h2>Please enter your email and password.</h2> */}
          <TextField
            onChange={(e) => {
              setEmail(e.target.value);
              setGeneralError("");
            }}
            className="input2"
            id="outlined-basic"
            label="Email"
            error={generalError}
            helperText={emailError}
            value={email}
            required
            variant="outlined"
          />

          <TextField
            onChange={(e) => {
              setPassword(e.target.value);
              setGeneralError("");
            }}
            value={password}
            className="input2"
            id="outlined-basic"
            label="Password"
            variant="outlined"
            required
            type="password"
            error={generalError}
            helperText={passwordError}
          />
          <Button
            className="loginBtn2"
            onClick={loginFunc}
            type="submit"
            variant="outlined"
            sx={{
              height: "6vh",
              width: "10vw",
              marginTop: "10px",
              fontWeight: "600",
              color: "var(--white)",
              backgroundColor: "var(--black)",
            }}
          >
            Submit
          </Button>
        </div>
      </Box>
    </div>
  );
};

export default Login;
