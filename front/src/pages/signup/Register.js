import React, { useState } from "react";
// import useEffect  from "react";
import "./register.css";
import { Button, Box, TextField, Alert } from "@mui/material";
import { useNavigate } from "react-router-dom";
import kingco from "../../Assets/logo.png";

// const URLReg = process.env.REACT_APP_URL + "/users/register";

const Register = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const navigate = useNavigate();

  async function login(e) {
    e.preventDefault();
    setGeneralError("");
    setEmailError("");
    setPasswordError("");
    setPhoneError("");

    // `${URLReg}`
    
    try {
      let item = { name, email, phone, password };  
      let result = await fetch(`/api/users/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      });
      localStorage.setItem("token", JSON.stringify(result.token));
      result = await result.json();
      console.log(result.token);
      if (result.message) {
        setGeneralError(result.message);
      } else {
        localStorage.setItem("token", JSON.stringify(result.token));
        localStorage.setItem("id", JSON.stringify(result._id));
        return navigate("/login");
      }
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="loginContainer">
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <div className="loginContainerform">
          {generalError && (
            <Alert
              className="err"
              severity="error"
              sx={{
                height: "60px",
                transform: "scale(1.3)",
                width: "16rem",
                position: "absolute",
                bottom: "33rem",
                boxShadow: "8",
              }}
            >
              {generalError}
            </Alert>
          )}
          {/* <h1>Votting App</h1> */}
          <img src={kingco} className="be2" alt="GI" />
          <TextField
            onChange={(e) => {
              setName(e.target.value);
              setGeneralError("");
            }}
            className="input1"
            id="outlined-basic"
            label="Full Name"
            error={generalError}
            helperText={emailError}
            required
            variant="outlined"
            value={name}
          />
          <TextField
            onChange={(e) => {
              setEmail(e.target.value);
              setGeneralError("");
            }}
            className="input2"
            id="outlined-basic"
            label="Email ..@.."
            error={generalError}
            helperText={emailError}
            required
            variant="outlined"
            value={email}
          />
          <TextField
            onChange={(e) => {
              setPhone(e.target.value);
              setGeneralError("");
            }}
            className="input2"
            id="outlined-basic"
            label="Phone number"
            error={generalError}
            helperText={phoneError}
            required
            variant="outlined"
            value={phone}
          />
          <TextField
            onChange={(e) => {
              setPassword(e.target.value);
              setGeneralError("");
            }}
            value={password}
            className="input3"
            id="outlined-basic"
            label="Password"
            variant="outlined"
            required
            type="password"
            error={generalError}
            helperText={passwordError}
          />
          <Button
            className="loginBtn"
            onClick={login}
            variant="outlined"
            sx={{
              height: "4vh",
              width: "10vw",
              marginTop: "3px",
              fontWeight: "600",
              color: "black",
              backgroundColor: "#CC9900",
              marginbottom:"3rem"
            }}
          >
            Sign Up
          </Button>
          <div className="notMemberBtn" onClick={() => navigate("/login")}>
            Already a member?{" "}
            <span className="notloggedBtn" onClick={() => navigate("/login")}>
              Log in
            </span>
            .
          </div>
        </div>
      </Box>
    </div>
  );
};

export default Register;
