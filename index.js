const express = require("express");
const colors = require("colors");
const dotenv = require("dotenv").config();
const { errorHandler } = require("./middleware/errorMiddleware");
const bodyParser = require("body-parser");
const connectDB = require("./config/db");
const path = require("path");
const cors = require("cors");

const port = process.env.PORT || 7000;

connectDB();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// mongoose.set('strictQuery', false);

app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({
//     extended: false
//   }));

//Serve static assets if in production
// if (process.env.Node_ENV === "production") {
//   //Set Static Folder
app.use(express.static(path.resolve(__dirname, './front/build')));
 
// }

 //Set Static Folder


app.use(cors());
app.use("/upload", express.static("upload"));
app.use("/api/users", require("./routes/userRoutes"));
app.use("/api/inf", require("./routes/influencerRoutes"));
app.use("/api/logo", require("./routes/logoRoutes"));
app.use("/api/logocard", require("./routes/logocardRoutes"));
app.use(errorHandler);
app.listen(port, () => console.log(`Server started on port ${port}`));
app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "front/build", "", "index.html"));
  });   
// "test": "echo \"Error: no test specified\" && exit 1",
