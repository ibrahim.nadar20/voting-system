const multer = require ("multer");
const  path =require ("path");
const PATH = "./upload";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./upload");
  },

  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

 
module.exports = upload = multer({ storage: storage });