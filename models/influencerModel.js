const mongoose = require("mongoose");

const influencerSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please add an Influencer"],
      unique: true,
    },
    voters: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],

  image:{
    type: String,     
  }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Influencer", influencerSchema);
