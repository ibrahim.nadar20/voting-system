const mongoose = require("mongoose");

const logoSchema = mongoose.Schema(
  {
    logo_header: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Logo", logoSchema);
