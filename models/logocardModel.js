const mongoose = require("mongoose");

const logocardSchema = mongoose.Schema(
  {
    logo_card: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Logocard", logocardSchema);
