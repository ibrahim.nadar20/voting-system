// const mongoose = require("mongoose");

// const userSchema = mongoose.Schema(
//   {
//     name: {
//       type: String,
//       required: [true, "Please add a name"]
//     },
//     email: {
//       type: String,
//       required: [true, "Please add an email"],
//       trim: true,
//       lowercase: true,
//       unique: true,
     
//     },
//     password: {
//       type: String,
//       required: [true, "Please add a password"],
//       trim: true,

//     },
//     isVote:{
//       type:Boolean,
//       default:false
//     }
//   },
//   {
//     timestamps: true
//   }
// );

// module.exports = mongoose.model("User", userSchema);


const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please add a name"]
    },
    email: {
      type: String,
      required: [true, "Please add an email"],
      unique: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: [true, "Please add a password"]
    },
    phone:{
      type:Number,
      required:[true, "Please add a phone nb"],
      unique: true,
    },
    isVote:{
      type:Boolean,
      default:false
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("User", userSchema);
