const express = require("express");
const router = express.Router();
const upload = require("../middleware/multer");
const {
  getInfluencer,
  setInfluencer,
  updateInfluencer,
  deleteInfluencer,
  // getInfluencerById,
  // getInf
} = require("../controllers/influencerController");
const { protect } = require("../middleware/authMiddleware");

router.get("/",  getInfluencer);
// router.get('/inf',protect,  getInf)
// router.get('/:id',protect, getInfluencerById)
router.post("/", upload.single("image"), setInfluencer);
router.post('/', setInfluencer)
router.put("/:id", updateInfluencer);
router.delete("/:id", deleteInfluencer);

module.exports = router;
