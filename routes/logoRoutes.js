const express = require("express");
const router = express.Router();
const upload = require("../middleware/multer");
const {
    getLogo,
    setLogo,
    updateLogo,
    deleteLogo
} = require("../controllers/logoController");
const { protect } = require("../middleware/authMiddleware");

router.post("/", upload.single("logo_header"),setLogo);
router.get("/",upload.single("logo_header"),getLogo);
// router.delete("/:id", deleteLogo);
// router.update("/:id", updateLogo);

module.exports = router;
