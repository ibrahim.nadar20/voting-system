const express = require("express");
const router = express.Router();
const upload = require("../middleware/multer");
const {
    getLogocard,
    setLogocard,
  
} = require("../controllers/logocardController");
const { protect } = require("../middleware/authMiddleware");

router.post("/", upload.single("logo_card"),setLogocard);
router.get("/",upload.single("logo_card"),getLogocard);
 


module.exports = router;
