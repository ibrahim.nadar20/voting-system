const express = require("express");
const router = express.Router();
const {
  registerUser,
  loginUser,
  getMe,
  getUsers,
  getUserById,
  deleteUser,
  vote,
} = require("../controllers/userController");
const { protect } = require("../middleware/authMiddleware");

router.post("/register", registerUser);
router.post("/login", loginUser);
router.post("/vote", protect, vote);
router.get("/me", protect, getMe);
router.get("/", protect,getUsers);
router.delete("/:id", deleteUser);
router.get("/:id", protect, getUserById);

module.exports = router;
